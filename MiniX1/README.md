## Carls Mini X 1

![](Skærmbillede_2023-02-12_kl._17.49.56.png)

[Klik her for at se mit program køre](https://carlvestbo.gitlab.io/aestetiskprogrammering/MiniX1/index.html)

[Klik her for at se koden](https://gitlab.com/carlvestbo/aestetiskprogrammering/-/blob/main/MiniX1/testsketch.js)

## Beskrivelse af programmet

Mit program består af 6400 cirkler i et 800 x 800 canvas, der skifter størrelse med funktionen random. Koden til mit program er egentligt forholdsvis kort, men ser ud af meget da jeg har brugt for-loops til at få så mange cirkler frem, i stedet for at skrive og placere hver enkelt cirkel i mit canvas. I min kode er der 80 cirkler på både x- og y-aksen. Og så er cirklens størrelse sat til at være random mellem 0 og 10, for at skabe et mere levende program der giver de mange cirkler en fed æstetik der for mig ligner lidt en stjernehimmel. Derudover benyttes de samme funktioner fra for-loopet også til min (r, g, b)-farver, hvor i++ ligger et tal oveni for hver cirkel, hvilket skaber en masse nuancer af farver på de mange cirkler. 


## En interessant oplevelse
Det var lidt ved et tilfælde at jeg prøvede at sætte cirklernes størrelse til at være random, men det blinkede så hurtigt at man fik helt ondt i øjnene, så jeg besluttede at bruge funktionen frameRate til at gøre programmet mere behageligt at kigge på. Denne funktion synes jeg var ret fed da den kan bruges til praktisk at løse et problem, men også til at give din kode et andet udtryk. Jeg eksperimenterede også med at sætte frameraten yderligere ned, hvilket gik programmet en videospil/8-bit æstetik som også var ret cool.


## Refleksioner omkring programmet og opgraderingsmuligheder
Jeg synes det har været sjovt at kode igen til den første Mini X opgave. Jeg har haft kodning lidt på hylden siden jeg lærte en smule om kodesproget processing på højskole i forbindelse med at lave digital kunst, så det hele lå lidt langt væk og jeg følte mig lidt rusten. Netop brugen af for-loops var noget af det jeg kunne huske, så det var også grunden til at jeg valgte dette. Til næste gang vil jeg gerne udfordre mig selv lidt mere og benytte nogle funktioner jeg aldrig har prøvet før! 
Men jeg synes også at programmet generelt er et godt udgangspunkt ift. de utallige muligheder man kan udforske indenfor kodning. I denne kode benyttes der blot simple former og tilfældige tal til at skabe et visuelt udtryk. Altsammen effektiviseret ved brugen af for-loops, der muliggør at man kan have et stort antal af et objekt, uden at de skal placeres enkeltvis, hvilket sparer programmøren for en masse tid. 
Hvis jeg skulle optimere min kode så kunne det være fedt at inkludere et interaktivt element. Dette kunne f.eks. være at man kunne ændre på frameraten eller cirklernes farve, hvilket ville skabe forskellige udtryk. Det kunne også være antallet af cirkler eller deres størrelse. Generelt synes jeg selv det er fedt når der er et interaktivt element, da man som bruger pludselig kommer ind og er medbestemmende ift. hvordan outputtet ser ud. Så det er bestemt noget jeg vil prøve at inkludere i de fremtidige opgaver!

Med kodningen, samt de tekster vi har læst i faget, er det virkeligt fedt at komme ind bagved skærmen og se hvordan de programmer vi bruger hver dag egentligt virker. Det går virkeligt op for en hvor meget der er bundet op på kode og hvordan kode hjælper os i alle mulige scenarier i vores hverdag. Jeg har i hvert fald lyst til at lære meget mere omkring både det at kode, samt de rammer vi koder indenfor. Og hvordan koden også kan bruges til at belyse komplekse problemstillinger i vores samfund. Mega spændende! Det skal nok blive fedt.
Jeg har været lidt nervøs overfor at skulle lære et helt nyt kodesprog, men synes at p5.js er virkeligt intuitivt og brugervenligt at arbejde med. Referencelisten gør det nemt og overskueligt at finde den funktion man leder efter, samt en beskrivelse af den så man faktisk forstår hvad det er man laver. Og på stack overflow kan man finde svar på alle ens kode-problemer, hvilket er rart, da det godt kan være frusterende når ens kode ikke virker. Lidt en øjenåbner at der er så stort et community indenfor kodning og der altid er hjælp at hente - Mega fedt!

Tror det var det for min første Mini X.
