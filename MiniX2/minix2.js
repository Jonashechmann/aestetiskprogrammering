let img1;
let img2;
let img3;


function preload() {
  img1 = loadImage('earth1.png');
  img2 = loadImage('earth2.png');
  img3 = loadImage('earth3.png');
}


function setup() {
  
  createCanvas(windowWidth,windowHeight);
  background(0,0,100);
  slider = createSlider(0, 17, 100);
  slider.position(windowWidth / 2 - 150, windowHeight / 2 +350);
  slider.style('width', '300px');
  slider.value(0);
  

}

function draw() {
  background(0,0,100);


  let val = slider.value();
  textSize(80);
  text(val, windowWidth / 2 + 200, windowHeight / 2 + 350);
  textSize(100);
  text('E.arth.MOJI', windowWidth / 2 - 250 , windowHeight / 2 - 300);
  textSize(50);
  text('WHAT IS YOUR', windowWidth / 2 + 300, windowHeight / 2 + 50 );
  text('CO2 FOOTPRINT', windowWidth / 2 + 300, windowHeight / 2 + 150 );
  text('PER YEAR?', windowWidth / 2 + 300, windowHeight / 2 + 250 );
  text('_____________', windowWidth / 2 + 300, windowHeight / 2 + 275);
  textSize(40);
  text('TON CO2 PER YEAR', windowWidth / 2 + 300, windowHeight / 2 + 350 );

if (val < 3) {
  image( img1, windowWidth / 2 - 250, windowHeight / 2 - 250, 500, 500);
  
  arc(windowWidth / 2, windowHeight / 2 + 100, 200, 200, 0, PI);
  
} else if ((val > 2) && (val < 8)) {
  image( img2, windowWidth / 2 - 250, windowHeight / 2 - 250, 500, 500);
  rect(windowWidth / 2 - 125, windowHeight / 2 - 30 + 100, 250, 70);
} else if ((val > 7) && (val < 17)) {
  image( img3, windowWidth / 2 - 250, windowHeight / 2 - 250, 500, 500);
  //arc(windowWidth / 2, windowHeight / 2 + 100, 200, 200, 0, PI);
  // Den kan ikke rotere, så det klarer vi lige med en push/pop
  push();
  translate(windowWidth / 2, windowHeight / 2 + 180);
  rotate(radians(180));
  arc(0, 0, 200, 200, 0, PI);
  pop();
} else if (val > 16) {
  image( img3, windowWidth / 2 - 250, windowHeight / 2 - 250, 500, 500);
  push();
  translate(windowWidth / 2, windowHeight / 2 + 180);
  rotate(radians(180));
  arc(0, 0, 200, 200, 0, PI);
  pop();
  textSize(100);
  stroke(0);
  fill(255,20,40);
  text('END OF HUMANITY', windowWidth / 2 - 450 , windowHeight / 2 + 70);



}
{
ellipse(windowWidth / 2 - 100, windowHeight / 2 - 100, 150);
fill(255);
ellipse(windowWidth / 2 + 100, windowHeight / 2 - 100, 150);
fill(0);
ellipse(windowWidth / 2 - 100, windowHeight / 2 - 100, 50);
fill(0);
ellipse(windowWidth / 2 + 100, windowHeight / 2 - 100, 50);
fill(255);



let link = createA('https://footprint.wwf.org.uk/', 'CHECK YOUR CO2 FOOTPRINT HERE');
link.position(100, 200);
link.parent('hjemmeside');


}
}
