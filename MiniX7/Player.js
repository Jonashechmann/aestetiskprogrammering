class Player {
    constructor(){
        this.posX = width/2;
        this.posY = height-50;
        this.speed = 50;
        this.size = 50;
    }

    moveLeft(){
        this.posX -= this.speed;
        move.play();
   
    }
    moveRight(){
        this.posX += this.speed;
        move.play();
       
    }

    show(){
        fill(255);
        stroke(5);
        rect(this.posX, this.posY, 120, 5);

    }
}

