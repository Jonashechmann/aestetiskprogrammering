function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(60);
  
  
}

function draw() {
  //Baggrund med alpha der giver 'fade-effekten'
  background(0, 8);
  
//Tekst til throbberen med en nice font XD
textAlign(CENTER);  
textSize(windowWidth / 50, windowHeight / 50);
textFont('Times New Roman');
fill(200);
text('Just a few more minutes',windowWidth / 2, windowHeight / 1.1);
 
//Centrer alt
  translate(width / 2, height / 2);
  
  //variabel a gør at cirklen starter i midten af skærmen 
  let a = windowHeight / 2;
  //variabel for cirklens hastighed
  let hastighed = 0.025;
  //Bruger cos og sin til at skabe uendelighedsmønstret
  x = a * sin(frameCount * hastighed);
  y = a * sin(frameCount * hastighed) * cos(frameCount * hastighed);

//Ellipsen med ovenstående position, samt farve
  noStroke();
  fill(75,0,255);
  // Crazy farver == lidt for meget
  //fill(random(0,150),random(0,250),255);
  ellipse(x, y, 100,50);

}