//en firkantblok
størrelse = 30;

widthFirkant = 5;
heightFirkant = 10;

function setup() {
  //rectMode(CENTER);
createCanvas(windowWidth,windowHeight);
strokeWeight(5);
background(0);

//Start-rgbværdier 
startR = random(0,165);
startG = random(0,165);
startB = random(0,165);

//nested forloop bruges til at lave de mange firkanter
for (i = width; i >  widthFirkant; i -= størrelse) {
  for (j = height; j >  heightFirkant; j -= størrelse) {

//farve med random
fill(random(startR, startR+90), random(startG, startG+90),random(startB, startB+90));

//tegner firkanterne med random, hvor de kan have en width op til 5 og en height op til 10
//floor-funktionen bruges til at konvertere de random-tal til hele tal
push();
translate(i + størrelse/2, j + størrelse/2);
rect(0,0, størrelse * floor(random(1,widthFirkant)), størrelse * floor(random(1,widthFirkant)))
pop();

  }
}
}
