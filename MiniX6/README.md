## Mini X 6

![](minix6.png)


[Klik her for at køre mit program](https://carlvestbo.gitlab.io/aestetiskprogrammering/MiniX6/index.html)

[Klik her for at se min kode](https://gitlab.com/carlvestbo/aestetiskprogrammering/-/blob/main/MiniX6/minix6.js)

[Klik her for at se min player Class](https://gitlab.com/carlvestbo/aestetiskprogrammering/-/blob/main/MiniX6/Player.js)

[Klik her for at se min bold Class](https://gitlab.com/carlvestbo/aestetiskprogrammering/-/blob/main/MiniX6/Tomato.js)


### Games with objects
Objective:

To implement a class-based, object-oriented sketch via abstracting and designing objects’ properties and methods.
To reflect upon object abstraction under the lived conditions of digital culture.

#### Beskrivelse af mit program 
Til denne mini x har jeg lavet min egen version af arkadespils-klassikeren "pong". Spillet fungerer ved at en bold flyver rundt på skærmen og hver gang den rammer en ydre væg af skærmen skifter den retning. I den nederste del af skærmen er playeren som egentligt bare er en plade, hvis formål er at bounce bolden videre og derved samle point, samt sørge for at bolden ikke ryger ned i jorden hvilket udløser GAME OVER. 


#### Beskrivelse af koden
Mit spil er bygget på "tofu game" som vi har arbejdet med i undervisningen. Spillet har samme opbygning med en Player class og en Tomato Class(som er den flyvende bold). Jeg har lavet bolden med tre `ellipse` i forskellige størrelser, som flyver rundt på en sort baggrund. Jeg har også tilføjet en smule Alpha på baggrunden for bedre at visualisere boldens retning. Derudover har jeg lavet fire `if-statements`i boldens Class som gør at bolden, afhængigt af hvilken væg den rammer, skal skifte retning.  


``````
// 4 if-statements for hver side i canvas der alle ganger med -1 for at skifte retning
    if (this.posX < 0){
        this.retningx = this.retningx * -1;
        blip.play();
        }
        if (this.posX > width){
        this.retningx = this.retningx * -1;
        blip.play();
        }
        if (this.posY < 0){
        this.retningy = this.retningy * -1;
        blip.play();
        }
        if (this.posY > height){
        this.retningy = this.retningy * -1;
        gameover.play();
        textAlign(CENTER);
        textSize(60);
        text("GAME OVER", width/2, height/2);
        fill(255);
        noLoop()
        }
        
```````

I det nederste `if-statement` skifter bolden ikke retning, men derimod vises teksten "GAME OVER" samt `noLoop` da man taber spillet hvis bolden rammer bunden. Det `if-statement` der ellers ville være når bolden rammer bunden er i stedet puttet ind i `function checkCollision`, hvilket gør at når bolden rammer Player så skifter den retning og man får et point.

```````
//laver funktionen collision der bestemmer at når spilleren og bolden kolliderer skal bolden bevæge sig i en anden retning
function checkCollision() {
    let distance;
    for (let i = 0; i < tomato.length; i++) {
        distance = dist(player.posX + 120/2, player.posY + 5/2, tomato[i].posX, tomato[i].posY);

        if (distance < 50-player.size/2) {
            tomato[i].retningy = tomato[i].retningy * -1;
            point++
            //pointLyd skal kun afspille hvis ikke den gør det i forvejen for at undgå den spiller oven i hinanden
            if(pointLyd.isPlaying() == false){
            pointLyd.play();
            }

````````

Derudover har jeg benyttet mig af p5.js lyd-bibliotek for første gang, hvor jeg indsat forskellige computerspilslyde, hvilket er med til at gøre spiloplevelsen mere autentisk, som hvis man sad i arkadehallen.
Det har dog drillet lidt med at få det til at virke...


#### Refleksioner

Spiludviklingen er kommet langt siden de første 8-bit-spil kom på markedet dengang min far var dreng. I dag er både konsollerne og de softwareprogrammer hvor spillene bliver udviklet på, så kraftfulde at det i dag er muligt at lave hyperrealistiske spil som gør det svært at afgøre om at det 3d-renderet eller virkeligt? I mit spil har jeg haft en "less is more" tilgang til det at skabe et spil. Jeg har forsøgt at skære ind til kernen og levere spillet i sin reneste form. Indenfor spiludvikling findes der mange grene som man kan have fokus som spildesigner. I mange nyere spil oplever jeg personligt at det for spilvirksomhederne i højere grad handler om at skabe det mest visuele flotte udtryk gennem deres spil, med f.eks. flotte vandsimulationer og vilde landskaber som skal gøre oplevelsen bedre. Men samtidig formår de samme spil ikke altid at have en rød tråd i spillets fortælling, hvilket giver et sløret billede af spillets præmis. I min egne spiloplevelser som jeg har haft, føler jeg at det vigtigste for et godt spil er at det vil fortælle en historie, både kompositorisk og visuelt, hvor de to elementer er i balance med hinanden. Det er fint at have et visuelt flot spil, men noget går tabt hvis ikke man får bygget det rette fundament som altid gode spil kræver. Det har også været spændende at arbejde med OOP, der gør det lettere for programmøren at holde overblik over sin kode og de forskellige elementer:
"Object-oriented programming provides a way to structure and organize code that reflects the structure of the problem domain. By breaking down complex systems into smaller, more manageable objects, it allows developers to write more modular and reusable code."


#### Referenceliste

* Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164
* https://p5js.org/reference/

